// adapted from http://en.wikipedia.org/wiki/Hilbert_curve
 
//convert d to (x,y)
function d2xy(n, d) {
  var p = {x: 0, y: 0};
  var rx, ry, s, t=d;
  for (s=1; s<n; s*=2) {
    rx = 1 & (t/2);
    ry = 1 & (t ^ rx);
    rot(s, p, rx, ry);
    p.x += s * rx;
    p.y += s * ry;
    t /= 4;
  }
  return p;
}
 
function rot(n, p, rx, ry) {
  if (ry == 0) {
    if (rx == 1) {
      p.x = n-1 - p.x;
      p.y = n-1 - p.y;
    }
    
    //Swap x and y
    var t  = p.x;
    p.x = p.y;
    p.y = t;
  }
}

var N = 4;
var M = 1 << N;
var SS = 17;
var RAD = 15;
var C = 2;
var frames_per_loop = 10;
var frames_per_complete_loop = frames_per_loop * C;

function lerp(a, b, t) {
  return b * t + a * (1-t);
}


function cconv(x) {
  var n = Math.floor(255 * x);
  if (n > 255) n = 255;
  if (n < 0) n = 0;
  return n;
}

function rgb(r, g, b) {
  return 'rgba(' + cconv(r) + ',' + cconv(g) + ',' + cconv(b) + ',1)'
}

function rr() {
  return Math.random() * 0.2 - 0.1;
}

function rgbr(r, g, b) {
  return 'rgba(' + cconv(r+rr()) + ',' + cconv(g+rr()) + ',' + cconv(b+rr()) + ',0.9)'
}

function rgba(r, g, b, a) {
  return 'rgba(' + cconv(r) + ',' + cconv(g) + ',' + cconv(b) + ',' + a + ')'
}

function mod(a, b) {
  var almost = a % b;
  return a < 0 ? (almost + b) % b : almost;
}

function drawThingy(dh, p, np, twl, nol) {  
  var gt = dh/(M * M);
  var special = !mod(dh - nol, C);
  if (special) {
    d.strokeStyle = rgba(0, 0, 0, 0.8);
  }
  else {
    d.strokeStyle = rgba(0, 0,0, 0.2);
  }
  d.beginPath();
  d.arc(w / 2 + (lerp(p.x, np.x, twl) - (M - 1) / 2) * SS,
	h / 2 + (lerp(p.y, np.y, twl) - (M - 1) / 2) * SS,
	RAD,0,2*Math.PI);
  d.stroke();
}

function step() {
  var z = (tt % frames_per_loop) / frames_per_loop;
  var t_within_loop = z > 0.5 ? 1 : (12 * z * z - 16 * z * z * z);
  var num_of_loops = Math.floor(tt / frames_per_loop);

  d.fillStyle = "#e9f9ff";
  d.fillRect(0,0,w,h);

  var p = d2xy(M, 0);
  var pp = {x: p.x - 1, y: p.y};
  drawThingy(-1, pp, p, t_within_loop, num_of_loops); 

  for (var dh = 0; dh < M * M - 1; dh++) {
    drawThingy(dh, d2xy(M, dh), d2xy(M, dh + 1), t_within_loop, num_of_loops);
  }

  var pe = d2xy(M, M * M - 1);
  var np = {x: pe.x + 1, y: pe.y};
  drawThingy(M * M - 1, pe, np, t_within_loop, num_of_loops); 
  if (tt < frames_per_complete_loop) {
    $.ajax({type: "POST", url: "/save", data: {tt: tt, img: c.toDataURL()}});
  }

}

c = $("#c")[0];
console.log('what');
d = c.getContext('2d');
w = c.width = (M + 1) * SS;
h = c.height = (M + 1) * SS;
d.fillStyle = "gray";

var tt = 0;
step(tt);
setInterval(function() { step(++tt); }, 40);