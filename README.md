![Hilbert Curve Animation](raw/36f887c22249f6a47e62b6c916f908fa706fcf21/anim.gif)

to render animation:

    node server.js
    chrome localhost:8080/a.html
    # wait a couple seconds for a complete loop through the animation
    convert -delay 2 -loop 0 /tmp/anim/*.png anim.gif