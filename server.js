var fs = require('fs');
var express = require('express'); // npm install express
var sprintf = require('sprintf'); // npm install sprintf

express()
  .use(express.static(__dirname))
  .use(express.bodyParser())
  .post('/save', function(req, resp) {
    var img = req.body.img.replace(/data:image\/png;base64,/, "");
    var buf = new Buffer(img, 'base64');
    fs.writeFileSync(sprintf('/tmp/anim/%03d.png', parseInt(req.body.tt)), buf);
    resp.end('ok');
  })
  .listen(8080);